# Example Test machine for 'Hello, world!' python application

This project creates docker [Ubuntu image](https://hub.docker.com/r/cs8w/ubuntu)
to run simple python [web application](https://gitlab.com/cs8w/python-hello)
through [Gunicorn](https://gunicorn.org/) WSGI HTTP Server
with [Supervisor](http://supervisord.org/) process control system
by [Ansible](https://www.ansible.com/) play.

## Syntax
### Runner
`01.runner.sh <command>`
The commands:
- `build` - build docker image
- `rebuild` - build docker image, force download layers
- `push` - push docker image to docker registry [hub.docker.com](http://hub.docker.com), I have uploaded it: `cs8w/testbench:v01`
- `start`- start docker container with prebuilt image (if `build` stage have been skipped, `cs8w/testbench:v01` from hub.docker.com is used)
- `status` - show info about running container
- `ansible` - run ansible play
- `exec` - execute shell command inside running container (i.e. exec bash)
- `stop` - kill the container

## Stages
### Building Docker image

The *test machine* uses _Ubuntu 16.04_ Linux image:
[Dockerfile](bench/Dockerfile) (simplified here in readme)
```Dockerfile
FROM ubuntu:16.04
LABEL maintainer="cs8w"

ARG ROOTPASS=PassWord1234

RUN apt-get update && apt-get install -y \
    openssh-server openssl curl wget git \
    netbase net-tools iputils-ping \
    sudo mc bash bash-completion locales

# Set locale to UTF-8
#
RUN locale-gen en_US.UTF-8 && /usr/sbin/update-locale LANG=en_US.UTF-8
RUN locale-gen ru_RU.UTF-8 && /usr/sbin/update-locale LANG=ru_RU.UTF-8
ENV LANGUAGE ru_RU.UTF-8
ENV LC_ALL ru_RU.UTF-8

RUN echo "root:$ROOTPASS" | chpasswd

CMD ["/usr/sbin/sshd", "-D"]
```
Image is created from Ubuntu 16.04.
While creating ssh-server, curl, sudo, git and locales packages are installed.
System language is set to Russian.
Also *root* password is set.
SSHd server is forced to start after container creation.

Keys for ssh are generated in this stage by bash script. Find it in [.ssh](.ssh) directory.

### Push
This stage pushes built docker image to docker registry. Use `docker login` before, and set `hub_id` variable to registry __user_id__.

### Start
This stage creates docker container from prebuilt image __cs8w/ubuntu:v01__ or downloads it from docker registry if build is skipped.
Running container name is set to *testbench* and _container_id_ is stored in `/tmp` - these prevent double instance.
SSH-keys generated before are used for authentication - `authorized_keys` is mounted to container while creating.
Script also updates Ansible [inventory](inventory.ini) for access to created *testbench*.

### Status
Shows information about running container: *container_id* and *ip-address*.

### Exec
Executes _shell command_ inside running container. For example:
```
./01.runner.sh exec bash
```
or 
```
./01.runner.sh exec ifconfig
```

### Ansible
Here Ansible play is performed.

### Stop
Simply stops running container and removes _container_id_ file.

## Ansible play

*Ansible* play uses:
- `ansible.cfg` - some settings for current project
- `inventory.ini` - access information to *testbench*
- `group_vars` - project variables
- `roles` - main play
- `ansible-setup.yml` - start _ansible-playbook_ with it

### Project variables
- app_name
- uuser

### Ansible performs:
Tasks are included by `main.yml` and gathered into these groups:
- prereq.yml - prerequisite. pip, virtualenv, openssl packages are installed here.
- mysql.yml - install *MySQL*, setup DB and access.
- adduser.yml - add new _unprivileged_ user for application running, generate and set password.
- webapp.yml - clone _Hello, World!_ __test app__ form [git repositiry](https://gitlab.com/cs8w/hello-python), create _log_ directory.
- virtualenv.yml - create and check python _virtual environment_ for user to start __test app__
- gunicorn.yml - install __gunicorn__ into _virtual env_
- systemd.yml - create _systemd_ configuration to run __test app__
- nginx.yml - install and setup __nginx__ as a proxy to socket, created by __gunicorn__ for __test app__
- supervisor.yml - install and setup __supervisor__ to run __test app__, __mysql__, __nginx__
- webappinit.yml - initialize __test app__ - create table for counter and put "1" into it.
- python.yml - download, compile and install version __3.5.4__
- webapp2.yml - create symlink to newer version of python inside virtual environment and force restart gunicorn

### Uncomment lines in production
Basicly, python app and unicorn are unsensitive to python version and last two stages of Ansible play are turned off by comments.
You should uncomment it for testing in [main.yml](roles/fullsetup/tasks/main.yml)
```
### upgrade python to v.3.5.4
#- include: python.yml
### restart gunicorn:webapp to use new python
#- include: webapp2.yml
```

## Some benchmarks
Whole __Ansible__ play lasts for 26 minutes, 21 of them belong to python compilation.
_It may be different on your equipment_.

## Hello, World! Application

I've written simple [web application](https://gitlab.com/cs8w/hello-python) on python.
It uses `Flask` to serve web requests and `MySQL` to store access counter.
File [requirements.txt](app/requirements.txt) is used to install modules `Flask` and `MySQL`.
Test run:
```
gunicorn wsgi:webapp -b localhost:8080 --workers 3 --access-logfile gunicorn.access.log
```
Application needs initialization by access it's URL with `/setup`, i.e.
```
curl http://localhost:8080/setup
```
Initialization creates database table for storing couner and sets it to '1'

Now __app__ is ready to serve requests:
```
curl http://localhost:8080
```

## Summary
Clone project, run test container, deploy test app, check (3x), destroy test container:
```
git clone https://gitlab.com/cs8w/testbench01.git
cd testbench01
./01.runner.sh start
./01.runner.sh status
./01.runner.sh ansible
./01.runner.sh geturl
./01.runner.sh geturl
./01.runner.sh geturl
./01.runner.sh stop
```
