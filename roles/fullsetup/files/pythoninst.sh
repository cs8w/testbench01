#!/bin/bash

apt-get install -y python3-pip #python3-venv

#apt-get install -y python3.5
# uninstall 3.5.2 version
#apt-get remove -y python3.5
#apt-get -y purge
#apt-get -y autoremove

#apt-get install -y python3-pip python3-openssl python3-venv

# install python 3.5.4
apt-get install -y gcc make build-essential \
        libssl-dev libffi-devel libc6-dev libnss3-dev zlib1g-dev libbz2-dev xz-utils tk-dev libmysql-dev libsqlite3-dev \
        libreadline-dev libreadline-gplv2-dev libncurses5-dev libncursesw5-dev libgdbm-dev

cd /usr/local/src
wget https://www.python.org/ftp/python/3.5.4/Python-3.5.4.tgz
tar zxvf Python-3.5.4.tgz
cd Python-3.5.4
#./configure --prefix=/usr --enable-optimizations --with-ssl=yes
./configure --enable-optimizations --with-ssl=yes --with-ensurepip=install
make -j 8
make altinstall
cd ..
rm Python-3.5.4.tgz
rm -rf Python-3.5.4

cd /usr/bin
ln -s /usr/local/bin/python3.5 python3.5.4
ln -s pip3 pip

#curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
#python3.5.4 get-pip.py

# create link to new version
#rm /home/webapp/webapp/env/bin/python3
#ln -s /usr/bin/python3.5.4 /home/webapp/webapp/env/bin/python3
# kill current python3 to force restart
#kill $(ps ax | grep python3 | awk {'print $1'})

