#!/bin/bash

hub_id="cs8w"
img_name="ubuntu"
img_ver="v01"
ins_name="testbench"

cmd="$1"

case "$cmd" in
  build)
    docker build -t ${hub_id}/${img_name}:${img_ver} ./bench
    #docker push     ${hub_id}/${img_name}:${img_ver}
    rm -f ./.ssh/*
    ssh-keygen -q -N "" -t rsa -f ./.ssh/id_rsa
    cat ./.ssh/id_rsa.pub > ./.ssh/authorized_keys
    exit 0
    ;;
  rebuild)
    docker build --no-cache --pull -t ${hub_id}/${img_name}:${img_ver} ./bench
    #docker push ${hub_id}/${img_name}:${img_ver}
    ;;
  push)
    docker push     ${hub_id}/${img_name}:${img_ver}
    exit 0
    ;;
  start)
    docker run -d -p 10.0.1.2:10022:22 -it --name ${ins_name} --mount type=bind,source="$(pwd)"/.ssh/authorized_keys,target=/root/.ssh/authorized_keys --cidfile /tmp/docker_test.cid ${hub_id}/${img_name}:${img_ver}
    docker container ls | grep ${ins_name}
    IP=`docker exec -it $(docker container ls | grep testbench | awk {'print $1'}) ifconfig | grep eth0 -A 1 | grep inet | awk {'print $2'} | awk -F":" {'print $2'}`
    ssh-keygen -R ${IP}
    echo IP: ${IP}
    cat <<EOF > inventory.ini
${IP} ansible_user=root ansible_ssh_private_key_file=./.ssh/id_rsa
[testbench]
${IP}
EOF
    ;;
  stop)
    cid=$(docker container ls -a | grep ${ins_name} | awk {'print $1'})
    docker container stop ${cid}
    docker container rm ${cid}
    rm -f /tmp/docker_test.cid
    ;;
  exec)
    cid=$(docker container ls -a | grep ${ins_name} | awk {'print $1'})
    docker exec -it ${cid} $2
    ;;
  ansible)
    ansible-playbook ansible-setup.yml
    ;;
  status)
    docker container ls | grep ${ins_name}
    [ -f "/tmp/docker_test.cid" ] && echo "Container ID `cat /tmp/docker_test.cid`" || echo "Container ID file is not created"
    ;;
  geturl)
    IP=`docker exec -it $(docker container ls | grep testbench | awk {'print $1'}) ifconfig | grep eth0 -A 1 | grep inet | awk {'print $2'} | awk -F":" {'print $2'}`
    curl ${IP}
    ;;
  *)
    echo "Usage: $0 {build|rebuild|push|start|stop|status|geturl|exec}" >&2
    exit 1
    ;;
esac

exit 0
