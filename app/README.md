## Hello, World! Application

This is a simple [web application](app.py) on python.
It uses `Flask` to serve web requests and `MySQL` to store access counter.
File [requirements.txt](requirements.txt) is used to install modules `Flask` and `MySQL`.
Test run:
```
gunicorn wsgi:webapp -b localhost:8080 --workers 3 --access-logfile gunicorn.access.log
```
Application needs initialization by access it's URL with `/setup`, i.e.
```
curl http://localhost:8080/setup
```
Initialization creates database table for storing couner and sets it to '1'
